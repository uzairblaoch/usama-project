import 'package:flutter/material.dart';
import 'package:shop_app/components/product_card.dart';
import 'package:shop_app/models/Product.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2.8;
    final double itemWidth = size.width / 2;
    return Scaffold(
      body: GridView.count(
        padding: EdgeInsets.only(right: 15),
        childAspectRatio: (itemWidth / itemHeight),
        shrinkWrap: true,
        // Create a grid with 2 columns. If you change the scrollDirection to
        // horizontal, this produces 2 rows.
        crossAxisCount: 2,
        // Generate 100 widgets that display their index in the List.
        children: [
          /*...List.generate(
            demoProducts.length,
            (index) {
              if (demoProducts[index].isPopular)
                return ProductCard(product: demoProducts[index]);

              return SizedBox.shrink(); // here by default width and height is 0
            },
          ),*/
        ],
      ),
    );
  }
}
