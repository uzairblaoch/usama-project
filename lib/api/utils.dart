import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shop_app/models/GetCategories.dart';
import 'package:shop_app/models/PopularProducts.dart';
import 'package:shop_app/models/productsByCategory.dart';

class Utils {
  final String baseUrl = 'grocerystore.codingoverflow.com';
  register(String first_name, String last_name, String email, String password,
      String confirm_password) async {
    var url = Uri.http(baseUrl, '/api/register', {"q": "dart"});
    final response = await http.post(url, body: {
      'first_name': first_name,
      'last_name': last_name,
      'email': email,
      'password': password,
      'password_confirmation': confirm_password
    });
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else if (response.statusCode == 500) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else {
      final String responseString = response.body;
      return jsonDecode(responseString);
    }
  }

  login(String email, String password) async {
    var url = Uri.http(baseUrl, '/api/login', {"q": "dart"});
    final response = await http.post(url, body: {
      "email": email,
      "password": password,
    });
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else if (response.statusCode == 401) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else if (response.statusCode == 500) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else {
      final String responseString = response.body;
      return jsonDecode(responseString);
    }
  }

  forgot(String email) async {
    var url = Uri.http(baseUrl, '/api/forgot', {"q": "dart"});
    final response = await http.post(url, body: {
      "email": email,
    });
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else if (response.statusCode == 400) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else if (response.statusCode == 404) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else if (response.statusCode == 500) {
      final String responseString = response.body;
      return jsonDecode(responseString);
    } else {
      final String responseString = response.body;
      return jsonDecode(responseString);
    }
  }

  checkToken(String email, String token) async {
    var url = Uri.http(baseUrl, '/api/checkToken', {'q': 'dart'});
    final response =
        await http.post(url, body: {'email': email, 'token': token});
    // print(email);
    // print(token);
    print(response);
    if (response.statusCode == 200) {
      final responseString = response.body;
      print(responseString);
      return jsonDecode(responseString);
    } else if (response.statusCode == 400) {
      final responseString = response.body;
      print(responseString);
      return jsonDecode(responseString);
    }
  }

  reset(String token, String password, String password_confirm) async {
    var url = Uri.http(baseUrl, '/api/checkToken', {'q': 'dart'});
    final response = await http.post(url, body: {
      'token': token,
      'password': password,
      'password_confirm': password_confirm
    });

    print(response);
    if (response.statusCode == 200) {
      final responseString = response.body;
      print(responseString);
      return jsonDecode(responseString);
    } else if (response.statusCode == 400) {
      final responseString = response.body;
      print(responseString);
      return jsonDecode(responseString);
    }
  }

  Future<GetCategories> fetchCategories() async {
    var url = Uri.http(baseUrl, '/api/categories', {"q": "dart"});
    final response = await http.get(url);
    print(response.body);
    return GetCategories.fromJson(jsonDecode(response.body));
  }

  Future<ProductsByCategory> productsByCategory(String category_name) async {
    var url = Uri.http(baseUrl, '/api/productsByCategory', {"q": "dart"});
    final response = await http.post(url, body: {
      "category_name": category_name,
    });
    print(response.body);
    return ProductsByCategory.fromJson(jsonDecode(response.body));
  }

  Future<GetPopularProducts> popularProducts() async {
    var url = Uri.http(baseUrl, '/api/getPopularProducts', {"q": "dart"});
    final response = await http.get(url);
    print(response.body);
    return GetPopularProducts.fromJson(jsonDecode(response.body));
  }
}
